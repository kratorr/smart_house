from __future__ import absolute_import, unicode_literals
from celery import task
import requests
from .models import Setting
from django.conf import settings
from .models import Setting
from ..settings import SMART_HOME_ACCESS_TOKEN, SMART_HOME_API_URL
from ..settings import EMAIL_RECEPIENT
from django.core.mail import send_mail
import json


@task()
def smart_home_manager():

    #context = {}
    try:
        headers = {'Authorization': 'Bearer ' + SMART_HOME_ACCESS_TOKEN}
        resp = requests.get(SMART_HOME_API_URL, headers=headers).json()
    except Exception as ex:
        pass

    bedroom_target_temperature  = Setting.objects.get(controller_name='bedroom_target_temperature').value
    hot_water_target_temperature = Setting.objects.get(controller_name='hot_water_target_temperature').value
    if resp['status'] == 'ok':
        otvet = {'controllers': []}
        state = dict([(i['name'], i['value']) for i in resp['data']])



        #1
        if state['leak_detector']:
            send_mail('test','протечка',EMAIL_RECEPIENT,[EMAIL_RECEPIENT])

            if state['cold_water']:
                otvet['controllers'].append({'name': 'cold_water', 'value': False})
                state['cold_water'] = False
            if state['hot_water']:
                otvet['controllers'].append({'name': 'hot_water', 'value': False})

        if state['cold_water'] and not state['smoke_detector']:
            if not state['boiler_temperature']:
                state['boiler_temperature'] = 0
            if state['boiler_temperature'] < hot_water_target_temperature * 0.9 and not state['boiler']:
                otvet['controllers'].append({'name': 'boiler', 'value': True})

            if state['boiler_temperature'] >= hot_water_target_temperature * 1.1 and state['boiler']:
                otvet['controllers'].append({'name': 'boiler', 'value': False})
            if state['washing_machine'] == 'broken':
                otvet['controllers'].append({'name': 'washing_machine', 'value': 'off'})
        else:
            if state['boiler']:
                otvet['controllers'].append({'name': 'boiler', 'value': False})
            if state['washing_machine'] != 'off':
                otvet['controllers'].append({'name': 'washing_machine', 'value': 'off'})
                state['washing_machine'] = 'off'

        if not state['smoke_detector']:
            if not state['bedroom_temperature']:
                state['bedroom_temperature'] = 0
            if not state['air_conditioner'] and state['bedroom_temperature'] > bedroom_target_temperature * 1.1:
                otvet['controllers'].append({'name': 'air_conditioner', 'value': True})
            if state['air_conditioner'] and state['bedroom_temperature'] < bedroom_target_temperature * 0.9:
                otvet['controllers'].append({'name': 'air_conditioner', 'value': False})
        else:
            if state['air_conditioner']:
                otvet['controllers'].append({'name': 'air_conditioner', 'value': False})
            if state['bedroom_light']:
                otvet['controllers'].append({'name': 'bedroom_light', 'value': False})
                state['bedroom_light'] = False
            if state['bathroom_light']:
                otvet['controllers'].append({'name': 'bathroom_light', 'value': False})
                state['bathroom_light'] = False
            if state['washing_machine'] != 'off':
                otvet['controllers'].append({'name': 'washing_machine', 'value': 'off'})

        if state['curtains'] != 'slightly_open':
            if state['outdoor_light'] < 50 and not state['bedroom_light']:
                if state['curtains'] != 'open':
                    otvet['controllers'].append({'name': 'curtains', 'value': 'open'})
            if (state['outdoor_light'] > 50 or state['bedroom_light']):
                if state['curtains'] != 'close':
                    otvet['controllers'].append({'name': 'curtains', 'value': 'close'})



        if len(otvet['controllers']) != 0:
            try:
                myy = json.dumps(otvet)
                result = requests.post(SMART_HOME_API_URL, data=myy, headers=headers)
            except:
                pass
        