from django.urls import reverse_lazy
from django.views.generic import FormView

from .models import Setting
from .form import ControllerForm

from django.core.serializers import serialize

from ..settings import SMART_HOME_ACCESS_TOKEN, SMART_HOME_API_URL
import requests
import json
from django.http import HttpResponse
from django.core.mail import send_mail
from django.shortcuts import render

class ControllerView(FormView):
    form_class = ControllerForm
    template_name = 'core/control.html'
    success_url = reverse_lazy('form')

    def dispatch(self, *args, **kwargs):
        
        try:
            headers = {'Authorization': 'Bearer ' + SMART_HOME_ACCESS_TOKEN}
            result = requests.get(SMART_HOME_API_URL, headers=headers).json()
        except:
            return HttpResponse(status=502)
        
        return super(FormView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ControllerView, self).get_context_data()
        controllers_data = {}
        try:
            headers = {'Authorization': 'Bearer ' + SMART_HOME_ACCESS_TOKEN}
            result = requests.get(SMART_HOME_API_URL, headers=headers).json()
        except:
            return HttpResponse(status=502)
        
        for i in result['data']:
            new_dict = {i['name'] : i['value']}
            controllers_data.update(new_dict)
        
        context['data'] = controllers_data #here data from controller
        return context


    def get_initial(self):
        try:
            bedroom_target_temperature  = Setting.objects.get(controller_name='bedroom_target_temperature')
            hot_water_target_temperature = Setting.objects.get(controller_name='hot_water_target_temperature')
        except:
            Setting.objects.update_or_create(controller_name='bedroom_target_temperature', value=21)
            Setting.objects.update_or_create(controller_name='hot_water_target_temperature', value=80)
            
        try:
            headers = {'Authorization': 'Bearer ' + SMART_HOME_ACCESS_TOKEN}
            resp = requests.get(SMART_HOME_API_URL, headers=headers).json()
        except:
            return HttpResponse(status=502)
        bedroom_light = None
        bathroom_light = None
        for i in resp['data']:
            if i['name'] == 'bedroom_light':
                bedroom_light = i['value']
            if i['name'] == 'bathroom_light':
                bathroom_light = i['value']

       
        return {'bedroom_target_temperature':bedroom_target_temperature.value,
                'hot_water_target_temperature': hot_water_target_temperature.value,
                'bedroom_light': bedroom_light,
                'bathroom_light': bathroom_light
        }

    def form_valid(self, form):
        
        try:
            headers = {'Authorization': 'Bearer ' + SMART_HOME_ACCESS_TOKEN}
            resp = requests.get(SMART_HOME_API_URL, headers=headers).json()
        except:
            pass

        
        try:
            obj = Setting.objects.get(controller_name='bedroom_target_temperature')
            obj.value = form.cleaned_data['bedroom_target_temperature']
            obj.save()
        except Setting.DoesNotExist:
            obj = Setting(controller_name='bedroom_target_temperature', value=21)
            obj.save()

        try:
            obj = Setting.objects.get(controller_name='hot_water_target_temperature')
            obj.value = form.cleaned_data['hot_water_target_temperature']
            obj.save()
        except Setting.DoesNotExist:
            obj = Setting(controller_name='hot_water_target_temperature', value=80)
            obj.save()


        otvet = {'controllers': []}
        controllers_data = dict([(i['name'], i['value']) for i in resp['data']])
        
        
        if form.cleaned_data['bedroom_light'] != controllers_data['bedroom_light']:
            
            if form.cleaned_data['bedroom_light'] == True:
                for i in resp['data']:
                    if i['name'] == 'smoke_detector':
                        if i['value'] == True:
                            pass
                        else:
                            otvet['controllers'].append({"name":"bedroom_light", "value": True})
            else:
                otvet['controllers'].append({"name":"bedroom_light", "value": False})

        if form.cleaned_data['bathroom_light'] != controllers_data['bathroom_light']:
            if form.cleaned_data['bathroom_light'] == True:
                for i in resp['data']:
                    if i['name'] == 'smoke_detector':
                        if i['value'] == True:
                            pass
                        else:
                            otvet['controllers'].append({"name":"bathroom_light", "value": True})
            else:
                otvet['controllers'].append({"name":"bathroom_light", "value": False})

        

        if len(otvet['controllers']) != 0:
            json_resp = json.dumps(otvet)
            try:
                resp = requests.post(SMART_HOME_API_URL, data=json_resp, headers=headers)
            except:
                return HttpResponse(status=502)

        return super(ControllerView, self).form_valid(form)